<?php
    include('global.inc.php');
    use \utils\ResponseUtil;
    use \utils\ParamUtil;


    $formPost = ParamUtil::RequireJSON($_POST,'form_data');
    $response = new ResponseUtil(ParamUtil::Get($_REQUEST,'format','json'),'demo');
    $response->WriteSimpleResponse(array('app-state'=>'awesome','results'=>$formPost));
    
       
?>
