<?php

#use utils\Calculations;
ini_set('display_errors', 1);
error_reporting(E_ALL);
//ini_set('html_errors', false);

define('BASEPATH', dirname(__FILE__) . DIRECTORY_SEPARATOR);

spl_autoload_register(function ($name) {

    $originalName = str_replace("\\", DIRECTORY_SEPARATOR, $name);
    $name = dirname(__FILE__) . DIRECTORY_SEPARATOR . "model" . DIRECTORY_SEPARATOR . $originalName . '.php';

    if (file_exists($name)) {
        require_once ($name);
    } else {
        $adodbTest = BASEPATH . 'php_lib/adodb/' . $originalName;
        if (file_exists($adodbTest)) {
            require_once ($adodbTest);
        } else {
            // throw new Exception('class '.$originalName.' not found');
        }
    }
});

$baseURL = "";
if (isset($_SERVER)) {
    if (isset($_SERVER['HTTP_HOST'])) {
        $baseURL .= ($_SERVER['SERVER_PORT'] == 80) ? 'http://' : 'https://';
        $baseURL .= $_SERVER['HTTP_HOST'];
    }
}
define('BASEURL', !isset($_SERVER['baseURL']) ? $baseURL . '/' : $baseURL . $_SERVER['baseURL']);
$params = $_REQUEST;
